-- NOTE: No plpgsql changes have been made with this release. Changes are in C & python code. This update is still required to allow the extension version upgrade chain to work.

-- Added support for PostgreSQL 9.0
-- Fix Makefile to not require explicit plpgsql file version declarations every update
